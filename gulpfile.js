/**
 *  (c) Nikita Sobolev: https://github.com/sobolevn
 *
 *  This software is released under the MIT License:
 *  http://www.opensource.org/licenses/mit-license.php
 */

/**
  To run locally for development:
  1. Open 'src/html/index.html'
  2. Edit 'src/js/map.js'

  Build command for production:
  gulp --url=https://map.idea-logic.ru --imagePath=https://map.idea-logic.ru/map build

  To build for test:
  1. Edit package version, append 'test' to current version number.
  2. Run:
  gulp --url=http://api.webtest.idea-logic.ru --imagePath=http://map.idea-logic.ru/map build
  3. Browse your results
  4. Remove all changes, run:
  git checkout -- .
*/

'use-strict';

var gulp = require('gulp');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename')
var package_json = require('./package.json');
var replace = require('gulp-replace');
var htmlreplace = require('gulp-html-replace');
var imagemin = require('gulp-imagemin');
var gutil = require('gulp-util');
// var escapeStringRegexp = require('escape-string-regexp');

var localUrl = gutil.env.localUrl || 'http://localhost:8000';
var envUrl = gutil.env.url || 'http://v2.api.idea-logic.ru';
var envImagePath = gutil.env.imagePath || 'http://map.idea-logic.ru/map';
envImagePath = envImagePath + '/dist/' + package_json.version + '/img';


gulp.task('build-images', function () {
  return gulp.src('src/img/*')
    .pipe(imagemin())
    .pipe(gulp.dest('dist/' + package_json.version + '/img'));
});


gulp.task('build-js', function() {
  return gulp.src('src/js/map.js')
    .pipe(replace(localUrl, envUrl))
    .pipe(replace(/'\.\.\/img';/, "'" + envImagePath + "';"))
    .pipe(gulp.dest('dist/' + package_json.version  + '/js'))
    .pipe(uglify())
    .pipe(rename('map.min.js'))
    .pipe(gulp.dest('dist/' + package_json.version + '/js'));
});


gulp.task('build-html', ['build-js'], function() {
  return gulp.src('src/html/index.html')
    .pipe(htmlreplace({
      js: {
        src: ['dist/' + package_json.version + '/js/map.min.js'],
        tpl: '<script src="%s" type="text/javascript"></script>'
      }
    }))
    .pipe(gulp.dest('.'));
});


gulp.task('build', ['build-js', 'build-html', 'build-images']);
