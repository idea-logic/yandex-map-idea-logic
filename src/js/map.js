(function($) {
   'use strict';

    $.fn.LBMap = function(options) {
        var settings = $.extend({}, $.fn.LBMap.defaults, options);
        var map = null;
        var placemarks = [];
        var payments = [];

        var content = '';

        var imagePath = '../img';
        var icon = imagePath + '/idea-logic-30px.png';
        var iconOff = imagePath + '/idea-logic-30px-off.png';
        var apiUrl = 'http://localhost:8000/rest/objects/pvz';

        var pickPoints = [];

        var setPickPoints = function(value) {
            pickPoints = value;
        };

        var inlineMap = function(obj) {
            var point = settings.defaultPoint;

            map = obj.get(0).map = new ymaps.Map(obj.get(0), {
                center: point,
                zoom: 10,
                behaviors: ['default', 'scrollZoom']
            });

            setDefaultPoint(point);

            var template = " \
            <div style='width: 300px;font-size: 9pt;'> \
                <h4>$[properties.id]:&nbsp;$[properties.name]</h4> \
                <div class='YMapsBaloonDescription'> \
                    <p><b>Address: </b>$[properties.address]</p> \
                    <p><b>Zip: </b>:&nbsp;$[properties.zip]</p> \
                    <p><b>Working hours: </b>:&nbsp;$[properties.working_hours]</p> \
                    <p align='right'> \
                        <a href='' class='choosePickPointButton' \
                            id='choosePickPoint-$[properties.id]' \
                            style='color: #37ab3e;'>Choose \
                        </a> \
                    </p> \
                </div> \
            </div>";
            content = ymaps.templateLayoutFactory.createClass(template, {});
            getPickPoints();
        };

        var getPickPoints = function() {
            settings.beforePickPointsDraw();
            var data = {};

            data['show'] = settings.show;
            data['pointTypes'] = settings.pointTypes;

            var request = $.ajax({
                method: 'GET',
                url: apiUrl,
                data: data,
                crossDomain: true,
                dataType: 'jsonp',
                cache: true
            });

            request.done(function(data, status) {
                setPickPoints(data);
                drawPickPoints();
                settings.afterPickPointsDraw(data, status);
            });

            request.fail(function(data, status) {
                settings.onPickPointsError(data, status);
            });
        };

        var setDefaultPoint = function(point) {
            map.setCenter(point, 10);
        };

        var drawPickPoints = function(mapFilters) {
            if (placemarks.length) {
                for (i in placemarks) {
                    map.removeOverlay(placemarks[i]);
                    pickPoints[i].placemark = null;
                };
                placemarks = [];
            }

            for (var i in pickPoints) {
                $('#pickPoint-' + pickPoints[i].id + '-link').removeClass('disabled');

                var pickPointIcon;
                if (pickPoints[i].is_working.length === 8) {
                    pickPointIcon = icon;
                } else {
                    pickPointIcon = iconOff;
                }

                placemarks[i] = new ymaps.Placemark([
                    pickPoints[i].lng, pickPoints[i].lat
                ], {
                    name: pickPoints[i].name,
                    working: pickPoints[i].working,
                    address: pickPoints[i].address,
                    id: pickPoints[i].id,
                    city: pickPoints[i].city,
                    working_hours: pickPoints[i].working_hours,
                    zip: pickPoints[i].zip,
                }, {
                    balloonContentLayout: content,
                    iconLayout: 'default#image',
                    // this is a very bad example of an API:
                    iconImageHref: pickPointIcon,
                    iconImageSize: [36, 35],
                    iconImageOffset: [-15, -35]
                });
                map.geoObjects.add(placemarks[i]);
                pickPoints[i].placemark = placemarks[i];
            }
            map.events.add('balloonopen', function(e) {
                $(document).on('click', '.choosePickPointButton', function() {
                    var split = $(this).attr('id').split('-');
                    selectPickPoint(split[1]);
                    return false;
                });
            });
        }

        function selectPickPoint(id) {
            for (var i in pickPoints) {
                if (pickPoints[i].id === id) {
                    settings.onPickPointSelect(pickPoints[i]);
                    break;
                };
            };
        }

        this.each(function() {
            var self = $(this);
            self.css({
                width: settings.width,
                height: settings.height
            });
            inlineMap(self);
        });
        return this;
    }

    $.fn.LBMap.defaults = {
        // New in '0.1.4':
        // "all|adm|pvz"
        "pointTypes": "all",

        // New in '0.1.3':
        // "all|working"
        "show": "working",

        "width": "540px",
        "height": "300px",
        "type": "inline",
        "showFilterBar": true,
        "showListBar": true,
        "YMapsKey": null,
        "defaultPoint": [55.76, 37.64],

        "beforePickPointsDraw": function() {},
        "afterPickPointsDraw": function(data, status) {},
        "onPickPointsError": function(data, status) {},
        "onPickPointSelect": function(pickPoint) {
            alert("NotImplementedError");
        }
    };
}(jQuery));

/*===========================
AMD Export
===========================*/
if (typeof(module) !== 'undefined') {
    module.exports = $.fn.LBMap;
}
else if (typeof define === 'function' && define.amd) {
    define([], function () {
        'use strict';
        return $.fn.LBMap;
    });
}
